
import tictactoe.client.controllers.CliController;
import tictactoe.client.controllers.GuiController;

public class TicTacToeClient
{

    public static void main(String[] args) {
        //GuiController guiController = new GuiController();
        CliController cliController = new CliController();
        cliController.connect();
    }
}
