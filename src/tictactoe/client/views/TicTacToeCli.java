package tictactoe.client.views;

import java.util.Scanner;

public class TicTacToeCli
{

    private final Scanner _scanner;

    public TicTacToeCli() {
        _scanner = new Scanner(System.in);
    }

    public String getUserInput(String message) {
        System.out.println(message);
        return _scanner.nextLine();
    }

    public void showMessage(String message) {
        System.out.println(message);
    }

    public void displayBoard(char[] board) {
        System.out.println();
        System.out.println(board[0] + " | " + board[1] + " | " + board[2]);
        System.out.println("-----------");
        System.out.println(board[3] + " | " + board[4] + " | " + board[5]);
        System.out.println("-----------");
        System.out.println(board[6] + " | " + board[7] + " | " + board[8]);
        System.out.println();
    }
}
