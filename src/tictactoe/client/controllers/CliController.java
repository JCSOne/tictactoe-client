package tictactoe.client.controllers;

import tictactoe.client.models.TicTacToeModel;
import tictactoe.client.views.TicTacToeCli;

public class CliController
{

    private TicTacToeCli _view;
    private TicTacToeModel _model;

    public CliController() {
        _view = new TicTacToeCli();
        _model = new TicTacToeModel();
    }

    public void connect() {
        int port = 0;
        while (port == 0) {
            try {
                port = Integer.parseInt(_view.getUserInput(
                        "Ingresa un número de puerto"));
            } catch (NumberFormatException e) {
                _view.showMessage("El número de puerto no es valido");
            }
        }

        char mark = '0';
        while (!(mark == 'X' || mark == 'O')) {
            mark = _view.getUserInput(
                    "dígita el caracter que quieres usar 'x' ó 'o'").toUpperCase().charAt(
                            0);
        }

        if (!_model.connect(_view.getUserInput(
                "Ingresa el servidor al que te quieres conectar"), port)) {
            _view.showMessage("Error estableciendo la conexión");
            return;
        }

        _model.setPlayerMark(mark);

        Thread gameThread = new Thread(_model);
        gameThread.start();

        _view.displayBoard(_model.getBoard());
        play();
    }

    private void play() {
        while (_model.playing()) {
            if (_model.getState().equals("wrongMove")) {
                _view.showMessage("La posición ya está ocupada");

            }
            if (_model.canPlay()) {
                int position = -1;
                while (position == -1) {
                    try {
                        position = Integer.parseInt(_view.getUserInput(
                                "Ingresa un número la posición del tablero en la que quieres jugar"));
                    } catch (NumberFormatException e) {
                        _view.showMessage(
                                "La posición del tableroen la que quieres jugar no es valida");
                    }
                }

                _model.setMove(position);
                listenToResponse();
            }
        }
    }

    private void listenToResponse() {
        while (true) {
            if (!_model.playing()) {
                _view.displayBoard(_model.getBoard());
                endMatch();
                break;
            } else if (_model.canPlay()) {
                _view.displayBoard(_model.getBoard());
                break;
            }
        }
    }

    private void endMatch() {
        switch (_model.getState()) {
            case "draw":
                _view.showMessage("El juego ha terminado en empate");
                break;
            case "youWon":
                _view.showMessage("Ganaste, fecilitaciones!");
                break;
            case "youLost":
                _view.showMessage("Perdiste, lo sentimos");
                break;
        }
    }
}
