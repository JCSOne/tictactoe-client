package tictactoe.client.controllers;

import tictactoe.client.models.TicTacToeModel;
import tictactoe.client.views.TicTacToeGui;

public class GuiController
{

    private final TicTacToeModel _model;
    private final TicTacToeGui _view;

    public GuiController() {
        _view = new TicTacToeGui(this);
        _view.setVisible(true);

        _model = new TicTacToeModel();
    }

    public void connect() {
        int port;
        try {
            port = Integer.parseInt(_view.getPortTxt());
        } catch (NumberFormatException e) {
            _view.showMessage("Error", "Ingrese un número de puerto valido");
            return;
        }

        char mark = '0';
        while (!(mark == 'X' || mark == 'O')) {
            mark = _view.getUserInput("Selección de caracter",
                    "dígita el caracter que quieres usar 'x' ó 'o'").toUpperCase().charAt(
                            0);
        }

        if (!_model.connect(_view.getHostTxt(), port)) {
            _view.showMessage("Error", "Error estableciendo la conexión");
            return;
        }

        _model.setPlayerMark(mark);

        Thread gameThread = new Thread(_model);
        gameThread.start();

        _view.setStartState();
        _view.updateBoard(_model.getBoard());
    }

    public void makePlay(int position) {
        _model.setMove(position);
        listenToResponse();
    }

    private void listenToResponse() {
        while (true) {
            if (!_model.playing()) {
                _view.updateBoard(_model.getBoard());
                endMatch();
                break;
            } else if (_model.canPlay()) {
                _view.updateBoard(_model.getBoard());
                break;
            }
        }
    }

    private void endMatch() {
        switch (_model.getState()) {
            case "draw":
                _view.showMessage("Tic Tac Toe",
                        "El juego ha terminado en empate");
                break;
            case "youWon":
                _view.showMessage("Tic Tac Toe", "Ganaste, fecilitaciones!");
                break;
            case "youLost":
                _view.showMessage("Tic Tac Toe", "Perdiste, lo sentimos");
                break;
        }
        _view.resetBoard();
    }
}
