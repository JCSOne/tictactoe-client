package tictactoe.client.models;

import tictactoe.client.views.TicTacToeGui;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class TicTacToeModel implements Runnable
{

    private BufferedReader _reader;
    private PrintWriter _writer;
    private Socket _socket;
    private Scanner _input;
    private char _playerMark;
    private char _computerMark;
    private char[] _board = new char[9];
    private boolean _playing;
    private boolean _canPlay = true;
    private String _state = "";

    public boolean connect(String host, int port) {
        try {
            Socket socket = new Socket(host, port);
            _socket = socket;
            _input = new Scanner(System.in);
            _reader = new BufferedReader(new InputStreamReader(
                    _socket.getInputStream()));
            _writer = new PrintWriter(_socket.getOutputStream(), true);
            _playing = true;
            _board = new char[9];
            return true;
        } catch (IOException ex) {
            return false;
        }
    }

    public void setPlayerMark(char mark) {
        _playerMark = mark;
        _computerMark = _playerMark == 'X' ? 'O' : 'X';
        _writer.println(mark);
    }

    public synchronized void setMove(int pos) {
        if (!_canPlay) {
            return;
        }
        _board[pos - 1] = _playerMark;
        _writer.println("move:" + pos);
        _canPlay = false;
    }

    public char[] getBoard() {
        return _board;
    }

    public boolean playing() {
        return _playing;
    }

    public boolean canPlay() {
        return _canPlay;
    }

    public String getState() {
        return _state;
    }

    @Override
    public void run() {
        String response;
        try {
            while (_playing) {
                response = _reader.readLine();
                if (response.contains("computerMove")) {
                    try {
                        int pos = Integer.parseInt(response.substring(13));
                        _board[pos] = _computerMark;
                        _canPlay = true;
                    } catch (NumberFormatException ex) {
                        // Ignored
                    }
                } else if (response.equals("wrongMove")) {
                    _state = response;
                    _canPlay = true;
                } else if (response.equals("draw") || response.equals("youWon")
                        || response.equals("youLost")) {
                    _state = response;
                    _playing = false;
                }
            }
            _socket.close();
        } catch (IOException ex) {
            // Ignored
        }
    }
}
